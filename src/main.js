
import { createApp } from 'vue'
import App from './canvas.vue'

createApp(App).mount('#app')
